function addElement() {
    let a = document.getElementsByTagName("input")[0].value;
    for (let i = 1; i < a; ++i) {
        // create a new div element
        const newButton = document.createElement("button");

        // and give it some content
        const newContent = document.createTextNode(`${i}`);

        // add the text node to the newly created div
        newButton.appendChild(newContent);

        // add the newly created element and its content into the DOM
        const currentButton = document.getElementById("button1");
        document.body.insertBefore(newButton, currentButton);

    }
}


